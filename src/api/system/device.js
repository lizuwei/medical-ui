import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listDevice(query) {
  return request({
    url: '/android/device/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getDevice(deviceId) {
  return request({
    url: '/android/device/' + deviceId,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addDevice(data) {
  return request({
    url: '/android/device',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateDevice(data) {
  return request({
    url: '/android/device',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delDevice(deviceId) {
  return request({
    url: '/android/device/' + deviceId,
    method: 'delete'
  })
}

// 导出【请填写功能名称】
export function exportDevice(query) {
  return request({
    url: '/android/device/export',
    method: 'get',
    params: query
  })
}

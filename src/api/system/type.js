import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listType(query) {
  return request({
    url: '/system/role/list',
    method: 'get',
    params: query
  })
}
// 查询【请填写功能名称】列表
export function MessageType(query) {
  return request({
    url: '/android/messageType/list',
    method: 'get',
    params: query
  })
}

// 查询分类名称列表
export function listTypeName(query) {
  return request({
    url: '/android/messageType/typeselect',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getType(typeId) {
  return request({
    url: '/android/messageType/' + typeId,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addType(data) {
  return request({
    url: '/android/messageType',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateType(data) {
  return request({
    url: '/android/messageType',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delType(typeId) {
  return request({
    url: '/android/messageType/' + typeId,
    method: 'delete'
  })
}

// 导出【请填写功能名称】
export function exportType(query) {
  return request({
    url: '/android/messageType/export',
    method: 'get',
    params: query
  })
}

import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listMessage(query) {
  return request({
    url: '/android/message/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getMessage(messageId) {
  return request({
    url: '/android/message/' + messageId,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addMessage(data) {
  return request({
    url: '/android/message',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateMessage(data) {
  return request({
    url: '/android/message',
    method: 'put',
    data: data
  })
}

// 资讯图片上传
export function uploadmessagePic(data) {
  return request({
    url: '/android/message/uploadmessagePic',
    method: 'post',
    data: data
  })
}

// 删除【请填写功能名称】
export function delMessage(messageId) {
  return request({
    url: '/android/message/' + messageId,
    method: 'delete'
  })
}

// 导出【请填写功能名称】
export function exportMessage(query) {
  return request({
    url: '/android/message/export',
    method: 'get',
    params: query
  })
}

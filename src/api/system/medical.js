import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listMedical(query) {
  return request({
    url: '/android/medical/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getMedical(medicalId) {
  return request({
    url: '/android/medical/' + medicalId,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addMedical(data) {
  return request({
    url: '/android/medical',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateMedical(data) {
  return request({
    url: '/android/medical',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delMedical(medicalId) {
  return request({
    url: '/android/medical/' + medicalId,
    method: 'delete'
  })
}

// 导出【请填写功能名称】
export function exportMedical(query) {
  return request({
    url: '/android/medical/export',
    method: 'get',
    params: query
  })
}

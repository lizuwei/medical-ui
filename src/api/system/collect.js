import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listCollect(query) {
  return request({
    url: '/android/collect/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getCollect(collectId) {
  return request({
    url: '/android/collect/' + collectId,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addCollect(data) {
  return request({
    url: '/android/collect',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateCollect(data) {
  return request({
    url: '/android/collect',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delCollect(collectId) {
  return request({
    url: '/android/collect/' + collectId,
    method: 'delete'
  })
}

// 导出【请填写功能名称】
export function exportCollect(query) {
  return request({
    url: '/android/collect/export',
    method: 'get',
    params: query
  })
}



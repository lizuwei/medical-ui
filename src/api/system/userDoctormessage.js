import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listMessage(query) {
  return request({
    url: '/android/UserDoctorMessage/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getMessage(mesId) {
  return request({
    url: '/android/UserDoctorMessage/' + mesId,
    method: 'get'
  })
}


// 查询【请填写功能名称】详细
export function researchDetail(mesUserId,mesDoctorId) {
  return request({
    url: '/android/UserDoctorMessage/researchDetail',
    method: 'get'
  })
}
// 新增【请填写功能名称】
export function addUserDoctorMessage(data) {
  return request({
    url: '/android/UserDoctorMessage/addUserDoctorMessage',
    method: 'post',
    data: data
  })
}


// 修改【请填写功能名称】
export function updateMessage(data) {
  return request({
    url: '/android/UserDoctorMessage',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delMessage(mesId) {
  return request({
    url: '/android/UserDoctorMessage/' + mesId,
    method: 'delete'
  })
}

// 导出【请填写功能名称】
export function exportMessage(query) {
  return request({
    url: '/android/UserDoctorMessage/export',
    method: 'get',
    params: query
  })
}

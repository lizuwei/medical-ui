import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listForum(query) {
  return request({
    url: '/android/forum/list',
    method: 'get',
    params: query
  })
}

export function uploadPic(data) {
  return request({
    url: '/android/forum/uploadPic',
    method: 'post',
    data: data
  })
}

// 查询【请填写功能名称】详细
export function getForum(forumId) {
  return request({
    url: '/android/forum/' + forumId,
    method: 'get'
  })
}

// 新增
export function addForum(data) {
  return request({
    url: '/android/forum/addForum',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateForum(data) {
  return request({
    url: '/android/forum/editForum',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delForum(forumId) {
  return request({
    url: '/android/forum/' + forumId,
    method: 'delete'
  })
}

// 导出【请填写功能名称】
export function exportForum(query) {
  return request({
    url: '/android/forum/export',
    method: 'get',
    params: query
  })
}

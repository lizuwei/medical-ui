import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listTemperature(query) {
  return request({
    url: '/android/temperature/forWeb',
    method: 'get'
  })
}


// 查询【请填写功能名称】详细
export function getTemperature(temperatureId) {
  return request({
    url: '/android/temperature/' + temperatureId,
    method: 'get'
  })
}


// 根据用户id查询最新十条
export function researchByUserId(userId) {
  return request({
    url: '/android/temperature/' + userId,
    method: 'get'
  })
}


// 新增【请填写功能名称】
export function addTemperature(data) {
  return request({
    url: '/android/temperature',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateTemperature(data) {
  return request({
    url: '/android/temperature',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delTemperature(temperatureId) {
  return request({
    url: '/android/temperature/' + temperatureId,
    method: 'delete'
  })
}

// 导出【请填写功能名称】
export function exportTemperature(query) {
  return request({
    url: '/android/temperature/export',
    method: 'get',
    params: query
  })
}

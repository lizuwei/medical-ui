import request from '@/utils/request'

// 查询列表
export function listHospital(query) {
  return request({
    url: '/android/hospital/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getHospital(hospitalId) {
  return request({
    url: '/android/hospital/' + hospitalId,
    method: 'get'
  })
}

export function uploadPic(data) {
  return request({
    url: '/android/hospital/uploadPic',
    method: 'post',
    data: data
  })
}

// 新增【请填写功能名称】
export function addHospital(data) {
  return request({
    url: '/android/hospital',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateHospital(data) {
  return request({
    url: '/android/hospital/edit',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delHospital(hospitalId) {
  return request({
    url: '/android/hospital/' + hospitalId,
    method: 'delete'
  })
}

// 导出【请填写功能名称】
export function exportHospital(query) {
  return request({
    url: '/android/hospital/export',
    method: 'get',
    params: query
  })
}

import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listWalk(query) {
  return request({
    url: '/android/walk/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getWalk(walkId) {
  return request({
    url: '/android/walk/' + walkId,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addWalk(data) {
  return request({
    url: '/android/walk',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateWalk(data) {
  return request({
    url: '/android/walk',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delWalk(walkId) {
  return request({
    url: '/android/walk/' + walkId,
    method: 'delete'
  })
}

// 导出【请填写功能名称】
export function exportWalk(query) {
  return request({
    url: '/android/walk/export',
    method: 'get',
    params: query
  })
}

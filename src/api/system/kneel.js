import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listKneel(query) {
  return request({
    url: '/android/kneel/list',
    method: 'get'
  })
}

// 查询【请填写功能名称】详细
export function getKneel(kneelId) {
  return request({
    url: '/android/kneel/' + kneelId,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addKneel(data) {
  return request({
    url: '/android/kneel',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateKneel(data) {
  return request({
    url: '/android/kneel',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delKneel(kneelId) {
  return request({
    url: '/android/kneel/' + kneelId,
    method: 'delete'
  })
}

// 导出【请填写功能名称】
export function exportKneel(query) {
  return request({
    url: '/android/kneel/export',
    method: 'get',
    params: query
  })
}

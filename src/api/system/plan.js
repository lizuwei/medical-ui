import request from '@/utils/request'

// 查询列表
export function listPlan(query) {
  return request({
    url: '/android/plan/list',
    method: 'get',
    params: query
  })
}

// 查询详细
export function getPlan(planId) {
  return request({
    url: '/android/plan/' + planId,
    method: 'get'
  })
}


export function uploadPic(data) {
  return request({
    url: '/android/plan/uploadPic',
    method: 'post',
    data: data
  })
}


// 新增
export function addPlan(data) {
  return request({
    url: '/android/plan/add',
    method: 'post',
    data: data,
  })
}

// 修改【请填写功能名称】
export function updatePlan(data) {
  return request({
    url: '/android/plan',
    method: 'put',
    data: data
  })
}


// 删除【请填写功能名称】
export function delPlan(planId) {
  return request({

    url: '/android/plan/' + planId,
    method: 'delete'
  })
}

// 导出【请填写功能名称】
export function exportPlan(query) {
  return request({
    url: '/android/plan/export',
    method: 'get',
    params: query
  })
}

import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listDoctor(query) {
  return request({
    url: '/android/doctor/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getDoctor(usdId) {
  return request({
    url: '/android/doctor/' + usdId,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addDoctor(data) {
  return request({
    url: '/android/doctor',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateDoctor(data) {
  return request({
    url: '/android/doctor',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delDoctor(usdId) {
  return request({
    url: '/android/doctor/' + usdId,
    method: 'delete'
  })
}

// 导出【请填写功能名称】
export function exportDoctor(query) {
  return request({
    url: '/android/doctor/export',
    method: 'get',
    params: query
  })
}
